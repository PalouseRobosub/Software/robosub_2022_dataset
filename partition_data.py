
import os

if __name__ == "__main__":
    files = os.listdir("./images/")
    files = filter(lambda file_name: file_name.endswith('.xml'), files)
    files = list(files)
    for f in files: 
        filenum = int(f[4:8])
        if filenum % 10 == 0:
            os.rename("./images/" + f, "./VALIDATION/" + f)
        elif filenum % 10 == 1:
            os.rename("./images/" + f, "./TEST/" + f)
        else:
            os.rename("./images/" + f, "./TRAINING/" + f)


        
